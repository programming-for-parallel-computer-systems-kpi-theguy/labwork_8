Laboratory work #8 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Graph structure creation mechanism, and using the group sending and receiving messages functions that was made in programming library of parallel computer systems with distributed memory - MPI.
Programming language: C++.
Used technologies: The MPI library stands for creating effective distributed parallel computer systems, even if it's structure comes as a bit unusual. This all works by creating special Graph of the processors in the system, to send messages with data between the nodes.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 13 threads, first one outputs the result.