//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #8------------------------------------
//------------MPI. Using graphs, Bcast, Scatter, Gather---------------------
//--------------------------------------------------------------------------
//----------Task: A = d * B + C * (MO * MK)---------------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 06.06.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include "data.h"
#include <iostream>


void input_Integer(int &a)
{
	a = 1;
}

void input_Vector(Vector &A)
{
	for (int i = 0; i < N; i++)
	{
		A[i] = 1;
	}
}

void input_Matrix(Matrix &MA)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			MA[i][j] = 1;
		}
	}
}

void output_Vector(const Vector &A)
{
	if (N < 15)
	{
		std::cout << std::endl;
		for (int i = 0; i < N; i++)
		{
			std::cout << A[i] << " ";
		}
		std::cout << std::endl;
	}
}

Matrix multiply_Matrixes(const Matrix &MA, const Matrix &MB)
{
	int cell;
	Matrix result(N, N);
	for (int i = 0; i < H; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cell = 0;
			for (int l = 0; l < N; l++)
			{
				cell += MA[i][l] * MB[l][j];
			}
			result[i][j] = cell;
		}
	}
	return result;
}

Vector multiply_Vector_Matrix(const Matrix &MA, const Vector &A)
{
	int cell;
	Vector result(N);
	for (int i = 0; i < H; i++)
	{
		cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[j] * MA[i][j];
		}
		result[i] = cell;
	}
	return result;
}

Vector multiply_Vector_Integer(const Vector &A, const int a)
{
	Vector result(N);
	for (int i = 0; i < H; i++)
	{
		result[i] = A[i] * a;
	}
	return result;
}

void sum_Vectors(const Vector &A, const Vector &B, Vector &C)
{
	for (int i = 0; i < H; i++)
	{
		C[i] = A[i] + B[i];
	}
}