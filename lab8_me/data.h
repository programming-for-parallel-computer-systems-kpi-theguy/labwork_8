//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #8------------------------------------
//------------MPI. Using graphs, Bcast, Scatter, Gather---------------------
//--------------------------------------------------------------------------
//----------Task: A = d * B + C * (MO * MK)---------------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 06.06.2018--------------------------------------------------
//--------------------------------------------------------------------------

#pragma once

#include <iostream>
extern int N;
extern int P;
const int H = N / P;

using namespace std;

class Vector
{
	int n;
	int* internal;

public:
	Vector(int n)
	{
		this->n = n;
		internal = new int[n];
	}

	Vector(const Vector &other)
	{
		this->n = other.n;
		internal = new int[n];
		memcpy(internal, other.internal, n * sizeof(int));
	}

	Vector& operator=(const Vector &other)
	{
		if (n != other.n)
		{
			delete[] internal;
			n = other.n;
			internal = new int[n];
		}
		memcpy(internal, other.internal, n * sizeof(int));
		return *this;
	}

	~Vector()
	{
		delete[] internal;
	}

	int operator[](int i) const
	{
		return internal[i];
	}

	int& operator[](int i)
	{
		return internal[i];
	}

	auto operator&() const -> void*
	{
		return reinterpret_cast<void*>(internal);
	}
};

class Matrix
{
	int m, n;
	int* internal;

public:
	Matrix(int m, int n)
	{
		this-> m = m;
		this-> n = n;
		internal = new int[m * n];
	}

	Matrix(const Matrix &other)
	{

		this->m = other.m;
		this->n = other.n;
		internal = new int[m * n];
		memcpy(internal, other.internal, m*n * sizeof(int));
	}

	Matrix& operator=(const Matrix &other)
	{
		if (m != other.m && n != other.n)
		{
			delete[] internal;
			m = other.m;
			n = other.n;
			internal = new int[m * n];
		}
		memcpy(internal, other.internal, m*n * sizeof(int));
		return *this;
	}

	~Matrix()
	{
		delete[] internal;
	}

	int* operator[](int i) const
	{
		return &internal[i*n];
	}

	auto operator&() const -> void*
	{
		return reinterpret_cast<void*>(internal);
	}
};

//Input Matrix, Vector, Integer procedures
void input_Integer(int &a);
void input_Vector(Vector &A);
void input_Matrix(Matrix &MA);

//Output Vector procedures
void output_Vector(const Vector &A);

//Multiplication functions
Matrix multiply_Matrixes(const Matrix &MA, const Matrix &MB);
Vector multiply_Vector_Matrix(const Matrix &MA, const Vector &A);
Vector multiply_Vector_Integer(const Vector &A, const int a);

//Sum procedure
void sum_Vectors(const Vector &A, const Vector &B, Vector &C);