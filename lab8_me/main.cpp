//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #8------------------------------------
//------------MPI. Using graphs, Bcast, Scatter, Gather---------------------
//--------------------------------------------------------------------------
//----------Task: A = d * B + C * (MO * MK)---------------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 06.06.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include <mpi.h>
#include "data.h"
#include <iostream>
#include <string>
#include <limits.h>

using namespace std;

int N = 13;
int P = 13;

int main(int argc, char *argv[])
{
	int rank;

	MPI_Init(&argc, &argv);

	int nodes = 13;
	int index[13] = {4,6,7,11,12,13,14,18,19,20,21,23,24};
	int edges[] = { 1,3,7,11, 0,2, 1, 0,4,5,6, 3, 3, 3, 0,8,9,10, 7, 7, 7, 0,12, 11};

	MPI_Comm graph;

	int reorder = 0;

	MPI_Graph_create(MPI_COMM_WORLD, nodes, index, edges, reorder, &graph);

	MPI_Comm_size(graph, &P);
	MPI_Comm_rank(graph, &rank);

	int d;
	Vector B_buf(N), C(N), A_buf(N);
	Matrix MO(N, N), MK_buf(N, N);
	Vector B(H), A(H);
	Matrix MK(H, N);

	cout << "T" << rank << "started" << endl;
	
	if (rank == 0)
	{
		//Input all values
		input_Integer(d);
		input_Vector(B_buf);
		input_Vector(C);
		input_Matrix(MO);
		input_Matrix(MK_buf);
	}
	//Send to all ranks d, C, MO
	MPI_Bcast(&d, 1, MPI_INT, 0, graph);
	MPI_Bcast(&C, N, MPI_INT, 0, graph);
	MPI_Bcast(&MO, N*N, MPI_INT, 0, graph);
	//Send to all ranks Bh, MKh
	MPI_Scatter(&B_buf, H, MPI_INT, &B, H, MPI_INT, 0, graph);
	MPI_Scatter(&MK_buf, H*N, MPI_INT, &MK, H*N, MPI_INT, 0, graph);

	//Calculation of math function
	sum_Vectors(multiply_Vector_Integer(B, d), multiply_Vector_Matrix(multiply_Matrixes(MK, MO), C), A);

	//Recieve all Ah from all ranks
	MPI_Gather(&A, H, MPI_INT, &A_buf, H, MPI_INT, 0, graph);

	if (rank == 0)
	{
		//Output the result
		output_Vector(A_buf);
	}

	cout << "T" << rank << "finished" << endl;

	MPI_Finalize();
    return 0;
}